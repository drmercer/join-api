# join-api

An unofficial command-line interface for [Join by Joaoapps](https://joaoapps.com/join/).

## Installation

```sh
$ npm install --global join-api
```

## Command-Line Usage

You will need your Join API key and device IDs, which you can get from the [Join web interface](https://joinjoaomgcd.appspot.com/).

To store your API key (this is important, sending won't work without it):
```sh
$ join-api set-key
Enter your API key: ******************************** # <-- Your API key
```

To add a device
```sh
$ join-api add-device <Your device ID> 'My Phone'
```

To send a URL to your device
```sh
$ join-api send --device 'My Phone' --url https://gitlab.com/
```

For full usage documentation, use `join-api -h` or `join-api help`.

## Usage in NodeJS

This package can also be imported as a module. Simply `require('join-api')`
