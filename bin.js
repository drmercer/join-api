#!/usr/bin/env node

const program = require('commander');
const prompt = require('password-prompt');
const main = require('./index.js');

program
	.description("A command-line interface for the Join API.");

program
	.command('set-key')
	.description('Set the API key to use')
	.action(doSafely(setApiKey))

function setApiKey() {
	return prompt('Enter your API key: ', {method: 'hide'})
	.then(doSafely(main.setApiKey));
}

program
	.command('add-device <device-id> [name]')
	.description('Add a device')
	.action(doSafely(main.addDevice))

program
	.command('list-devices')
	.description('List all devices')
	.action(doSafely(main.listDevices))

program
	.command('remove-device <name>')
	.description('Remove all devices with names matching <name> pattern (supports regex)')
	.action(doSafely(main.removeDevice))

program
	.command('send')
	.option('-d, --device <id|name>', 'the Device to send to')
	.option('-c, --copy <text>', 'Sets the device clipboard (and writes text if an app is open)')
	.option('-u, --url <url>', 'sends a URL')
	.option('-t, --text <text>', 'sets notification text or Tasker command')
	.option('-T, --title <title>', 'sets notification title')
	.action(doSafely(main.send))

program
	.command('clear-key')
	.description('Clear the stored API key')
	.action(doSafely(main.clearApiKey))

program
	.command('help [command]')
	.action(commandName => {
		if (!commandName) {
			program.help();
		} else {
			const c = program.commands.find(cmd => cmd.name() === commandName);
			if (c) c.help();
			else console.log("Command '%s' not found", commandName);
		}
	})

// If a command is unrecognized, output help information
program
	.command('*', null, { noHelp: true })
	.action(() => program.help())

if (process.argv.length < 3) {
	program.help();
}

program.parse(process.argv);

function doSafely(fn) {
	return function(...args) {
		try {
			fn(...args, err => {
				if (err) console.error("Error:", err.message || err);
				else console.log("Success!");
			});
		} catch (err) {
			console.error("Error:", err.message || err);
		}
	}
}
