const Conf = require('conf');
const buildUrl = require('build-url');
const request = require('request');

const config = new Conf({
	encryptionKey: "thIs-Is-AN-eNcRypTIon-KEY",
	defaults: {
		devices: [],
	},
});

function setApiKey(key) {
	key = key.trim().toLowerCase();
	if (!/[a-f0-9]{32}/.test(key)) {
		throw new Error("Invalid key; must be a 32-character hex string.");
	}
	config.set('api-key', key);
}

function clearApiKey() {
	config.delete('api-key');
}

function addDevice(deviceId, name) {
	const devices = config.get('devices');
	name = name || ("Device " + (devices.length+1));
	devices.push({
		id: deviceId,
		name: name,
	});
	config.set('devices', devices);
	console.log("Added device '%s'", name);
}

function listDevices() {
	config.get('devices').forEach(d => {
		console.log("%s (ID: %s)", d.name, d.id);
	});
}

function removeDevice(name) {
	const regex = new RegExp(name);
	const devices = config.get('devices');
	config.set('devices', devices.filter(d => !regex.test(d.name)));
}

function _findDevice(search) {
	const regex = new RegExp(search, 'i');
	const devices = config.get('devices');
	const dev = devices.find(d => d.name.match(regex));
	return dev || devices.find(d => d.id.match(regex));
}

function send(opts, cb) {
	if (!opts.device) throw new Error("No device specified.");
	const dev = _findDevice(opts.device);
	if (!dev) throw new Error("No device found for '"+opts.device+"'.");

	const query = {};
	query.deviceId = dev.id;
	opts.copy && (query.clipboard = opts.copy);
	opts.url && (query.url = opts.url);
	opts.title && (query.title = opts.title);
	opts.text && (query.text = opts.text);
	const url = _makeQueryURL(query);

	request(url, (err, res, body) => {
		const statusCode = res && res.statusCode;

		try {
			body = body && JSON.parse(body);
		} catch (ignored) {
			// Body's not JSON
		}

		if (err || statusCode !== 200 || body.errorMessage) {
			// Assemble error information
			if (body && body.errorMessage) {
				err = new Error(body.errorMessage);
			} else {
				err = err || new Error("Request failed. Status: " + statusCode);
			}
			err.statusCode = statusCode;
			Object.assign(err, body);
			cb(err);
		} else {
			cb();
		}
	});
}

const URL_BASE = 'https://joinjoaomgcd.appspot.com/_ah/api/messaging/v1/sendPush';
function _makeQueryURL(queryParams) {
	queryParams.apikey = config.get('api-key');
	if (!queryParams.apikey) {
		throw new Error("API key not set!");
	}
	return buildUrl(URL_BASE, { queryParams });
}

module.exports = {
	setApiKey,
	clearApiKey,
	addDevice,
	listDevices,
	removeDevice,
	send,
}
